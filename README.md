# Cookiecutters

Homework templates for use with [Cookiecutter](https://cookiecutter.readthedocs.io/).

[asciinema](https://asciinema.org) terminal casts for the templates:

- [Homework 1](https://asciinema.org/a/L6uVLMrXR3O9IwsSxYaCO1pqc)
